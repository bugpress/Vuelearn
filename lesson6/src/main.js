import Vue from 'vue'
import App from './App.vue'

//请求数据
import VueResource from 'vue-resource';
Vue.use(VueResource);

//引入vue-router
import VueRouter from 'vue-router';
Vue.use(VueRouter);

//创建组件
import Home from './components/Home.vue';
import News from './components/News.vue';
import Content from './components/Content.vue';
// import Pcontent from './components/Pcontent.vue';
import User from './components/User.vue'
    import UserAdd from './components/User/UserAdd.vue';
    import UserList from './components/User/UserList.vue';
//配置路由，注意名字
const routes = [
  {path:'/home',component:Home},
  //记住js跳转的时候这里要加上name的属性
  {path:'/news',component:News,name:'news'},
  {path:'/user',component:User,
    children:[
      {path:'useradd',component:UserAdd},
      {path:'userlist',component:UserList}
    ]
  },
  { path: '/content/:aid', component: Content },   /*动态路由*/
  // { path: '/pcontent', component: Pcontent },
  //设置默认的跳转路由
  {path:'*',redirect:'/home'}
]


//实例化VueRiuter
const router = new VueRouter({
  //hash模式改为history
  mode:'history',
  routes // （缩写）相当于 routes: routes
})
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
