####学习历程


一.首先搭建vue的开发环境（lesson1）

1.必须安装nodejs

2.搭建vue的开发环境，安装vue的脚手架工具(初次安装vue环境的时候)

npm install -global vue-cli

3.创建vue项目

    vue init webpack filename

    cd filename

    npm install  /  cnpm install

    npm run dev

另一种创建方式：

    vue init webpack-simple filename

    cd filename

    npm install  /  cnpm install

    npm run dev

二.绑定数据、绑定对象、循环数组渲染数据（lesson2）

三.绑定属性，绑定class，绑定style

四.双向数据绑定

    MVVM  vue就是一个MVVM的框架，model改变会影响视图view,view视图反过来影响model
    
    双向数据绑定必须在表单里面使用

五.Vue事件：定义方法，执行方法，获取数据，改变数据执行方法传值，以及事件对象

六.todolist双向数据绑定和状态的变化（lession3）

七.本地存储模块化以及封装Storage组件实现保存搜索的历史记录（lesson4）

八.Vue组件和生命周期函数【组件挂载、以及组件更新、组件销毁的时候触发的一系列方法,这些方法就叫生命周期函数】（lesson4）

九.数据请求，使用vue-resource请求数据，需要安装，在main.js中引入import VueResource from 'vue-resource';Vue.use(VueResource);在组件直接使用this.$http.get(地址).then(function(){...})（lesson4）

十.使用axios请求数据,与上面的区别是请求的不同，一个是body，另一个是data.(lesson4)

十一.父组件给子组件传值的方法,子组件里面通过 props接收父组件传过来的数据（lesson4）

十二.父组件主动获取子组件的数据和方法[调用子组件的时候定义一个ref，this.$refs.组件.方法;]，子组件主动获取父组件的数据和方法[ this.$parent.方法/数据]（lesson5）

十三.非父子组件传值(要引入一个vue的实例，即model里面的VueEvent.js,进行监听)（lesson5）

十四.Vue的路由以及路由跳转,要安装vue-router,在main.js里面配置vue-router和组件（lesson6）

十五.动态路由get传值,两种方式（1.<router-link :to="'/pcontent?id='+key">{{key}}--{{item}}</router-link>对应的路由： { path: '/pcontent', component: Pcontent }，2种： <router-link :to="'/content/'+key">{{key}}--{{item}}</router-link>对应的路由：{ path: '/content/:aid', component: Content },   /*动态路由*/）（lesson6）


十六.路由结合请求数据实现列表渲染（lesson6）

十七.路由编程式导航（通过js跳转），路由history模式以及默认的hash模式（lesson6--home.vue）（lesson6）

十八.嵌套式路由,父组件路由下面有一个子路由children[..路由]（lesson6）

十九.Vue UI框架Mint UI的使用  button  索引列表  ActionSheet等（lesson7）