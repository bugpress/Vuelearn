// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Mint from 'mint-ui'
import 'mint-ui/lib/style.css'
Vue.use(Mint)


//请求数据

import VueResource from 'vue-resource';
Vue.use(VueResource);


import VueRouter from 'vue-router';

Vue.use(VueRouter);

Vue.config.productionTip = false

//创建组件

import Home from './components/Home.vue';

import News from './components/News.vue';

import Content from './components/Content.vue';


import User from './components/User.vue';

//2.配置路由   注意：名字

const routes = [
  { path: '/home', component: Home },
  { path: '/news', component: News,name:'news' },

  { path: '/user', component: User},
 
  { path: '/content/:aid', component: Content },   /*动态路由*/

  { path: '*', redirect: '/home' }   /*默认跳转路由*/
]


//3.实例化VueRouter  注意：名字

const router = new VueRouter({
  mode: 'history',   /*hash模式改为history*/
  routes // （缩写）相当于 routes: routes
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
