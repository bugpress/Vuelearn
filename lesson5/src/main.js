import Vue from 'vue'
import App from './App.vue'

//引入vue-router
import VueRouter from 'vue-router';
Vue.use(VueRouter);

//创建组件
import Home from './components/Home.vue';
import News from './components/News.vue';

//配置路由，注意名字
const routes = [
  {path:'/home',component:Home},
  {path:'/news',component:News},
  //设置默认的跳转路由
  {path:'*',redirect:'/home'}
]


//实例化VueRiuter
const router = new VueRouter({
  routes // （缩写）相当于 routes: routes
})
new Vue({
  el: '#app',
  render: h => h(App)
})
